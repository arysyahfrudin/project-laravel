<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\film;
use App\cast;
use App\genre;
use File;

class filmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre=genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:film',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster'=>'mimes:jpeg,jpg,png|max:2200'
        ]);

        $gambar = $request->poster;
        $new_gambar = time().'-'.$gambar->getClientOriginalName();

        $film=film::create([
            'judul'=>$request->judul,
            'ringkasan'=>$request->ringkasan,
            'tahun'=>$request->tahun,
            'genre_id'=>$request->genre_id,
            'poster'=>$new_gambar,
        ]);

        $gambar->move('uploads/film/',$new_gambar);
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::findorfail($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre=genre::all();
        $film = film::findorfail($id);
        return view('film.edit', compact('film' , 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|unique:film',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster'=>'mimes:jpeg,jpg,png|max:2200'
        ]);
        $film = film::findorfail($id);

        if($request->has('poster')) {
            $path="uploads/film/";
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $new_gambar= time().'-'.$gambar->getClientOriginalName();
            $gambar->move('uploads/film/', $new_gambar);
            $film_data = [
                'judul'=>$request->judul,
                'ringkasan'=>$request->ringkasan,
                'tahun'=>$request->tahun,
                'genre_id'=>$request->genre_id,
                'poster'=>$new_gambar,
            ];
        }else {
            $film_data = [
                'judul'=>$request->judul,
                'ringkasan'=>$request->ringkasan,
                'tahun'=>$request->tahun,
                'genre_id'=>$request->genre_id,
            ];
        }
        $film->update($film_data);

        return redirect()->route('film.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film= film::findorfail($id);
        $film->delete();

        $path = "uploads/film/";
        File::delete($path . $film->poster);

        return redirect()->route('film.index');
    }
}
