<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('page.table');
});

Route::get('/data-table', function () {
    return view('page.data-table');
});

route::get('/cast','CastController@index');
route::get('/cast/create','CastController@create');
route::post('/cast','CastController@store');
route::get('/cast/{cast_id}','CastController@show');
route::get('/cast/{cast_id}/edit','CastController@edit');
route::put('/cast/{cast_id}','CastController@update');
route::delete('/cast/{cast_id}','CastController@destroy');

Route::resource('film', 'filmController');

Route::resource('genre', 'genreController');