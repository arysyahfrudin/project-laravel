@extends('layout.master')

@section('judul')
Tambah Film
@endsection
@section('isi')


<div>
        <form action="/film/{{$film->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Judul Film</label>
                <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul" value="{{$film->judul}}">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Ringkasan</label>
                <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tahun</label>
                <input type="number" class="form-control" name="tahun" id="body" placeholder="Masukkan Tahun" value="{{$film->tahun}}">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Genre</label>
                <select class="form-control" name="genre_id" id="exampleFormControlSelect1">
                    <option value="">--Pilih Genre--</option>
                    @foreach ($genre as $item)
                        @if ($item->id === $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>    
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                    @endforeach
                </select>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Poster</label>
                <input type="file" class="form-control-file" name="poster">
              </div>
              @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection