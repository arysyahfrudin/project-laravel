
@extends('layout.master')

@section('judul')
<h2>Show Film {{$film->id}}</h2>
@endsection
@section('isi')

    <img class="card-img-top" src="{{asset('uploads/film/'.$film->poster)}}" alt="Card image cap">

    <h2>{{$film->judul}}</h2>
    <p>{{$film->ringkasan}}</p>

@endsection