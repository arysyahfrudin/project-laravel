
@extends('layout.master')

@section('judul')
    List Film
@endsection
@section('isi')

<a href="/film/create" class="btn btn-primary mb-3">Tambah file</a>

<div class="row">
    @foreach ($film as $value)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('uploads/film/'.$value->poster)}}" alt="Card image cap">
            <div class="card-body">
            <h5 class="card-title mb-3">{{$value->judul}} ({{$value->tahun}})</h5>
            <p class="card-text">{{Str::limit($value->ringkasan, 100)}}</p>
            <a href="/film/{{$value->id}}" class="btn btn-primary">Detail</a>
            <a href="/film/{{$value->id}}/edit" class="btn btn-info">Edit</a>
            <form action="/film/{{$value->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger mt-2">
            </form>
        </div>
    </div>
</div>        
    @endforeach
    
</div>

    
@endsection