@extends('layout.master')

@section('judul')
Edit Genre {{$genre->id}}
@endsection
@section('isi')


<div>
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Genre</label>
                <input type="text" class="form-control" name="nama" id="title" value="{{$genre->nama}}" placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>

@endsection